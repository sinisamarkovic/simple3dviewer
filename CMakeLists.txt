﻿cmake_minimum_required (VERSION 3.23)

# Set the vcpkg toolchain file
set(CMAKE_TOOLCHAIN_FILE ${CMAKE_CURRENT_SOURCE_DIR}/vcpkg/scripts/buildsystems/vcpkg.cmake
    CACHE STRING "Vcpkg toolchain file")
set(VCPKG_MANIFEST_MODE ON)

# As part of Simple3DViewer I wanted to make a template for a C++ monorepo
project("Monorepo")

# VCPKG dependencies
find_package(glfw3 CONFIG REQUIRED)
find_package(imgui CONFIG REQUIRED)
find_package(assimp CONFIG REQUIRED)

# External dependencies that are embedded
add_subdirectory(external/glad)
add_subdirectory(external/ImGuiFileDialog)

# One and only project
add_subdirectory(Simple3DViewer)