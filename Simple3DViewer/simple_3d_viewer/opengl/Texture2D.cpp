#include <simple_3d_viewer/opengl/Texture2D.hpp>

#include <format>
#include <iostream>

namespace Simple3D {
static GLuint createTexture(const ImageData &image);

Texture2D::Texture2D(const ImageData &image) {
  texture_ = createTexture(image);
}

void Texture2D::use(uint32_t textureSlot) const {
  glActiveTexture(GL_TEXTURE0 + textureSlot);
  glBindTexture(GL_TEXTURE_2D, texture_);
}

void Texture2D::release() {
  if (texture_) {
    glDeleteTextures(1, &texture_);
    texture_ = 0;
  }
}

static GLuint createTexture(const ImageData &image) {
  GLuint texture{};
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

  int colorChannelCount = image.colorChannelCount;
  GLenum format{};
  if (colorChannelCount == 4)
    format = GL_RGBA;
  else if (colorChannelCount == 3)
    format = GL_RGB;
  else if (colorChannelCount == 1)
    format = GL_RED;
  else {
    throw std::invalid_argument(
        std::format("Invalid number of color channels for texture at path {}!",
                    image.path));
  }

  glTexImage2D(GL_TEXTURE_2D, 0, format, image.width, image.height, 0, format,
               GL_UNSIGNED_BYTE, image.image.get());

  glBindTexture(GL_TEXTURE_2D, 0);

  return texture;
}
} // namespace Simple3D
