#pragma once

#include <glad/glad.h>

#include <simple_3d_viewer/utils/ImageData.hpp>

namespace Simple3D {
class Texture2D {
public:
  Texture2D(const ImageData &image);
  Texture2D(const Texture2D &) = delete;
  Texture2D(Texture2D &&other) noexcept { swap(*this, other); }
  Texture2D &operator=(const Texture2D &) = delete;
  Texture2D &operator=(Texture2D &&other) noexcept {
    if (this == &other) {
      return *this;
    }

    release();

    swap(*this, other);
    return *this;
  }
  ~Texture2D() { release(); }

  void use(uint32_t textureSlot = 0) const;
  void release();

  friend void swap(Texture2D &lhs, Texture2D &rhs) {
    std::swap(lhs.texture_, rhs.texture_);
  }

private:
  GLuint texture_ = 0;
};
} // namespace Simple3D
