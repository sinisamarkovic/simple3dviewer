#pragma once

#include <filesystem>
#include <format>
#include <fstream>
#include <string>

#include <simple_3d_viewer/utils/ScopeGuard.hpp>

namespace Simple3D {
inline std::string loadFileIntoString(const std::string &filePath) {
  std::ifstream in(filePath, std::ios::in | std::ios::binary);
  if (in) {
    std::string file;
    in.seekg(0, std::ios::end);
    file.resize(in.tellg());
    in.seekg(0, std::ios::beg);
    in.read(&file[0], file.size());
    in.close();
    return file;
  }
  const std::filesystem::path path(filePath);
  throw std::invalid_argument(
      std::format("Shader at path {} doesn't exist!",
                  std::filesystem::absolute(path).string()));
}
} // namespace Simple3D
