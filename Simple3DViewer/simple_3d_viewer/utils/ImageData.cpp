#include <simple_3d_viewer/utils/ImageData.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#include <format>
#include <string>

namespace Simple3D {
void FreeImageDestructor::operator()(uint8_t *image) { stbi_image_free(image); }

ImageData loadImage(const std::string &filePath, bool flip) {
  if (flip) {
    stbi_set_flip_vertically_on_load(1);
  }
  int width{};
  int height{};
  int colorChannelCount{};
  uint8_t *loadedImage =
      stbi_load(filePath.c_str(), &width, &height, &colorChannelCount, 0);
  if (!loadedImage) {
    throw std::invalid_argument(
        std::format("Image at path {} doesn't exist!", filePath));
  }
  return {ScopeGuard<uint8_t, FreeImageDestructor>(loadedImage), filePath,
          width, height, colorChannelCount};
}
} // namespace Simple3D
