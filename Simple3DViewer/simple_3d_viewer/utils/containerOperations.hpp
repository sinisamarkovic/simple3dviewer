#pragma once

#include <vector>

namespace Simple3D {
template <typename T> size_t getTotalSize(const std::vector<T> &v) {
  return v.size();
}

template <typename T, typename... Rest>
size_t getTotalSize(const std::vector<T> &first, const Rest &...rest) {
  return first.size() + getTotalSize(rest...);
}

template <typename T>
void concat(std::vector<T> &result, const std::vector<T> &first) {
  result.insert(result.end(), first.begin(), first.end());
}

template <typename T, typename... Rest>
void concat(std::vector<T> &result, const std::vector<T> &first,
            const Rest &...rest) {
  result.insert(result.end(), first.begin(), first.end());
  concat(result, rest...);
}

template <typename T, typename... Rest>
std::vector<T> concatenate(const std::vector<T> &first, const Rest &...rest) {
  std::vector<T> result;
  result.reserve(getTotalSize(first, rest...)); 
  concat(result, first, rest...);
  return result;
}
} // namespace Simple3D
