#include <simple_3d_viewer/rendering/Material.hpp>

namespace Simple3D {
uint64_t Material::idGenerator_ = 0;
using TextureType = TextureData::Type;
const std::unordered_map<TextureType, std::string> textureTypeToString = {
    {TextureType::Diffuse, "diffuse"},
    {TextureType::Specular, "specular"},
    {TextureType::Emissive, "emissive"},
    {TextureType::Normals, "normals"},
    {TextureType::Metalness, "metalness"},
    {TextureType::DiffuseRoughness, "diffuseRoughness"}};

static void assignSlotsToTextures(const std::vector<TextureData> &textures,
                                  Program &program);
static void setMaterialUniforms(Material &material, Program &program);

void Material::use(Program &program) {
  assignSlotsToTextures(textures, program);
  setMaterialUniforms(*this, program);
}

static void setSamplerUniform(Program &program,
                              const std::string &uniformNameBase,
                              uint32_t concatNumber, uint32_t slot) {
  const std::string uniformName =
      uniformNameBase + std::to_string(concatNumber);
  program.doOperations([&uniformName, slot](Program &program) {
    program.setInt(uniformName, slot);
  });
}

static void setUVChannelUniform(Program &program,
                                const std::string &uniformNameBase,
                                uint32_t concatNumber,
                                const std::optional<int> &uvChannel) {
  const int uvChannelValue = uvChannel.value_or(concatNumber);
  const std::string uniformName =
      uniformNameBase + std::to_string(concatNumber);
  program.doOperations([&uniformName, uvChannelValue](Program &program) {
    program.setInt(uniformName, uvChannelValue);
  });
}

static void assignSlotsToTextures(const std::vector<TextureData> &textures,
                                  Program &program) {
  std::unordered_map<TextureType, size_t> counts;
  for (size_t i = 0; i < textures.size(); ++i) {
    const auto &textureData = textures[i];
    counts[textureData.type] += 1;
    textureData.texture->use(i);
    setSamplerUniform(program,
                      textureTypeToString.at(textureData.type) + "Texture",
                      counts[textureData.type], i);
    setUVChannelUniform(program,
                        textureTypeToString.at(textureData.type) + "UVChannel",
                        counts[textureData.type], textureData.uvChannel);
  }
}

static std::unordered_map<TextureType, size_t>
getTextureTypeToCountMap(const std::vector<TextureData> &textures) {
  std::unordered_map<TextureType, size_t> result;
  for (const auto &[type, _] : textureTypeToString) {
    result[type] = 0;
  }

  for (const auto &texture : textures) {
    result[texture.type] += 1;
  }

  return result;
}

static void setMaterialUniforms(Material &material, Program &program) {
  program.doOperations([&material](Program &program) {
    program.setVec3f("ambientColor", material.ambientColor);
    program.setVec3f("diffuseColor", material.diffuseColor);
    program.setVec3f("specularColor", material.specularColor);
    program.setVec3f("emissiveColor", material.emissiveColor);

    program.setFloat("shininess", material.shininess);
    program.setFloat("shininessStrength", material.shininessStrength);

    const auto textureTypeToCount = getTextureTypeToCountMap(material.textures);
    for (const auto [textureType, count] : textureTypeToCount) {
      program.setInt(textureTypeToString.at(textureType) + "TexturesCount",
                     static_cast<int>(count));
    }
  });
}
} // namespace Simple3D
