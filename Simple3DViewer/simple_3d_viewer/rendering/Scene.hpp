#pragma once

#include <optional>

#include <simple_3d_viewer/linear_algebra/meshGeneration.hpp>
#include <simple_3d_viewer/opengl/Program.hpp>
#include <simple_3d_viewer/opengl/TextureCubeMap.hpp>
#include <simple_3d_viewer/rendering/Camera.hpp>
#include <simple_3d_viewer/rendering/Mesh.hpp>
#include <simple_3d_viewer/rendering/Model.hpp>

namespace Simple3D {
inline std::string modelProgramFilename = "model";

struct Scene {
  Scene(Size framebufferSize);

  Camera camera;
  Mesh light;
  Program lightProgram;
  glm::vec3 lightPosition{};
  Mesh skybox;
  Program skyboxProgram;
  TextureCubeMap skyboxTexture;
  std::optional<Model> model;
  Program modelProgram;
};
} // namespace Simple3D
