#pragma once

#include <simple_3d_viewer/rendering/Mesh.hpp>

namespace Simple3D {
Mesh generateSphereMesh(float radius);
Mesh generateSkyboxMesh();
} // namespace Simple3D
