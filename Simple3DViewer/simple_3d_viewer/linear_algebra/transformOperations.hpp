#pragma once

#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include <simple_3d_viewer/utils/Size.hpp>
#include <simple_3d_viewer/linear_algebra/Transform.hpp>

namespace Simple3D {
glm::mat4x4 calculateModelTransform(const Transform& transform);
glm::mat4x4 calculateProjectionTransform(Size framebufferSize);
glm::mat4x4 calculateViewTransform(const glm::vec3 &position,
                                   const glm::vec3 &orientation,
                                   const glm::vec3 &up);
} // namespace Simple3D
